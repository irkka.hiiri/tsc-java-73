package ru.tsc.ichaplygina.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectService.findAll(UserUtil.getUserId()));
    }

}
