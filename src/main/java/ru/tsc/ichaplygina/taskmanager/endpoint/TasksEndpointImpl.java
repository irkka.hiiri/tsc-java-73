package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.TasksEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.ichaplygina.taskmanager.api.TasksEndpoint")
public class TasksEndpointImpl implements TasksEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @Nullable
    @WebMethod
    @PostMapping("/add")
    public List<Task> add(@RequestBody @WebParam(name = "tasks") @NotNull List<Task> tasks) {
        return taskService.write(UserUtil.getUserId(), tasks);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/remove")
    public void remove(@RequestBody @WebParam(name = "tasks") @NotNull List<Task> tasks) {
        taskService.remove(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/removeAll")
    public void removeAll() {
        taskService.removeAll(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    @PutMapping("/save")
    public List<Task> save(@RequestBody @WebParam(name = "tasks") @NotNull List<Task> tasks) {
        return taskService.write(UserUtil.getUserId(), tasks);
    }

}
