package ru.tsc.ichaplygina.taskmanager.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskEndpointTest {

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    private static final String TASKS_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final Task task1 = new Task("Test Task 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2");

    @NotNull
    private final Task task3 = new Task("Test Task 3");

    @NotNull
    private final Task task4 = new Task("Test Task 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        add(task1);
        add(task2);
    }

    @After
    @SneakyThrows
    public void clean() {
        mockMvc.perform(MockMvcRequestBuilders.delete(TASKS_URL + "removeAll")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final Task task) {
        @NotNull String url = TASK_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void addAll(@NotNull final List<Task> tasks) {
        @NotNull String url = TASKS_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        add(task3);
        @Nullable final Task task = findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.getId(), task.getId());

    }

    @Test
    @Category(UnitCategory.class)
    public void testAddAll() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task3);
        tasks.add(task4);
        addAll(tasks);
        @Nullable final Task task = findById(task1.getId());
        Assert.assertNotNull(findById(task3.getId()));
        Assert.assertNotNull(findById(task4.getId()));
    }

    @SneakyThrows
    private Task findById(@NotNull final String id) {
        @NotNull String url = TASK_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(findById(task1.getId()));
    }

    @SneakyThrows
    private List<Task> findAll() {
        @NotNull final String url = TASKS_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeById() {
        @NotNull String url = TASK_URL + "removeById/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeList() {
        @NotNull String url = TASKS_URL + "remove";
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(1, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeAll() {
        @NotNull String url = TASKS_URL + "removeAll";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void save() {
        @NotNull final String url = TASK_URL + "save/";
        @NotNull final Task task = task1;
        task.setName("New Name");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        this.mockMvc.perform(MockMvcRequestBuilders.put(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final Task newTask = findById(task.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals("New Name", newTask.getName());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void saveList() {
        @NotNull final String url = TASKS_URL + "save/";
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        tasks.get(0).setName("New Name");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        this.mockMvc.perform(MockMvcRequestBuilders.put(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final Task newTask = findById(tasks.get(0).getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals("New Name", newTask.getName());
    }

}
